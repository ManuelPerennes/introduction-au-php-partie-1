<!-- VI superglobales, sessions et cookies

Dans tous les exercices, faites une page HTML 5 valide et soignez vos CSS.

Exercice 1 Faire une page HTML permettant de donner à l'utilisateur :

    son User Agent
    son adresse ip
    le nom du serveur -->
<?php
echo $_SERVER['HTTP_USER_AGENT'] . "\n\n";
echo "<br>";
echo $_SERVER['REMOTE_ADDR'];
echo "<br>";
echo gethostname(); 
?>
<br>
<!-- Exercice 2 Sur la page index, faire un liens vers une autre page. Passer d'une page à 
l'autre le contenu des variables nom, prenom et age grâce aux sessions.
 Ces variables auront été définies directement dans le code. 
 Il faudra afficher le contenu de ces variables sur la deuxième page. -->
cf contact
<br>
<!-- Exercice 3 Faire un formulaire qui permet de récupérer le login et le mot de passe 
de l'utilisateur. A la validation du formulaire, stocker les informations dans un cookie. -->
<?php
?>
<br>
<!-- Exercice 4 Faire une page qui va récupérer les informations du cookie
 créé à l'exercice 3 et qui les affiches. -->
 <?php
?>
<br>
<!-- Exercice 5 Faire une page qui va pouvoir modifier
 le contenu du cookie de l'exercice 3. -->
 <?php
?>
<br>