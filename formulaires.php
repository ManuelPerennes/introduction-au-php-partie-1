
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    

<!-- V Les formulaires -->

<!-- Prenez soin d'agrémenter votre code avec de l'HTML 5 valide (DOCTYPE, ....) -->

<!-- **Exercice 1 **Créer un formulaire demandant le nom et le prénom.
 Ce formulaire doit rediriger vers la page user.php avec la méthode GET. -->
<form method="GET" action="user.php" > 
    <p>
        <label for="nom"> Votre nom :</label></br>
        <input type="text" name="nom" placeholder=" "> 
    </p>
    <p>
        <label for="prenom"> Votre prenom :</label></br>
        <input type="text" name="prenom" placeholder=" "> 
    </p>
    <p>
         <input type="submit" value="Envoyer">
    </p>
 </form>
<?php

?>
<br>
<!-- Exercice 2 Créer un formulaire demandant le nom et le prénom.
 Ce formulaire doit rediriger vers la page user.php avec la méthode POST. -->
 <form method="POST" action="user.php" > 
    <p>
        <label for="nom"> Votre nom :</label></br>
        <input type="text" name="nom" placeholder=" "> 
    </p>
    <p>
        <label for="prenom"> Votre prenom :</label></br>
        <input type="text" name="prenom" placeholder=" "> 
    </p>
    <p>
         <input type="submit" value="Envoyer">
    </p>
 </form>

