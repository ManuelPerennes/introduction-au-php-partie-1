<!-- Exercice 1 Créer une variable et l'initialiser à 0. 
Tant que cette variable n'atteint pas 10 :

    l'afficher  
    incrémenter de 1 -->

<?php $i= 0 ;
 for ($i= 0; $i <= 10 ; $i++ ) {
     if ($i != 10)
     echo $i ;
 }
?>

<br>




<!-- Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. 
Tant que la première variable n'est pas supérieur à 20 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    incrémenter la première variable -->

   <?php 
    $i = 0;
    $k = 2;
  
  
while ($i <= 20){
   
    $j = $i*$k;
    echo $j;
    $i++;
   
   
}
    ?>
<br>
<!-- Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un
 nombre compris en 1 et 100.
 Tant que la première variable n'est pas inférieur ou égale à 10 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    décrémenter la première variable -->

    <?php
    
$i = 100 ; 
$J= 60;
while ($i>=10) {
    $k= $i*$j ;
    echo $k ; 
    $i--;
}
    ?>
<br>
<!-- Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 
10 :

    l'afficher
    l'incrementer de la moitié de sa valeur -->
    <?php
    $i=1;
    while ($i <=10 ){

        echo $i;
        echo "<br>";
        $i = $i + ($i/2);
    }


    ?>
<br>
<!-- Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque... -->
<?php
$i = 0;
while ($i <=15 ) {
$i++;
echo "On y arrive presque...";
echo "<br>";
}
?>

<!-- Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon... -->
<?php

$i=20;
while ($i != 0){
$i--;
echo "C'est presque bon..";
echo "<br>";
}
?>
<br>

<!-- Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout... -->
<?php
$i = 1;
while ($i <=100){
    $i=$i+15;
    echo "on tient le bon bout";
    echo "<br>";
}
?>
<br>
<!-- Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! ! -->
<?php
$i=200;
while ($i >= 0){
   $i=$i-12;
    echo"Enfin";
    echo "<br>";
}
?>